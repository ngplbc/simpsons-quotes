import React from 'react';
import logo from './logo.svg';
import './App.css';

import Quotes from "./Quotes";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      working: false
    }
  }

  handleClick = () => {
    this.setState({
      working: !this.state.working
    })
  }; 

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className={this.state.working ? "App-logo-scale" : "App-logo"} alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <button onClick={this.handleClick}>Scale me!</button>
        </header>
        <Quotes />
      </div>
    );
  }
}

export default App;
